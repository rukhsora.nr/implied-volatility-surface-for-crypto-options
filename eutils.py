import datetime
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from typing import Literal, Union, Dict, Tuple, Optional

# internal
# from generic.chain_data import ChainData
from prop.generic_vol_slices import ExpirySlice
from stochvolmodels.utils import plots as plot
import seaborn as sns


def get_fig_size(nb_subplots: int):
    nb_subplots = int(np.ceil(nb_subplots/2)) * 2
    FIG_SIZE = {2: (14, 6),
                4: (18, 12),
                6: (18, 18),
                8: (18, 24)}
    if nb_subplots not in FIG_SIZE:
        raise ValueError('unknown number of subplots. Cannot determinte the size')
    return FIG_SIZE[nb_subplots]

def get_theme():
    return "whitegrid"


def to_yearfrac(d1, d2):
    return (d2 - d1).days / 365.0


# def diff1(func, x: np.ndarray, eps: float) -> np.ndarray:
#     deriv_right = 1.0 / eps * (func(x + eps) - func(x))
#     deriv_left = 1.0 / eps * (func(x) - func(x - eps))
#     deriv_central = 0.5 / eps * (func(x + eps) - func(x - eps))
#     np.where(np.logical_or(x < -eps, x > eps), deriv_central)
#     res = np.where(x > 0.5*eps, deriv_right, deriv_left)
#     res = np.where(np.logical_and(x < 0.5*eps, x > -0.5*eps), deriv_average, res)
#     return res
#
#
# def diff2(func, x: np.ndarray, eps: float) -> np.ndarray:
#     deriv_right = 1.0 / eps * (func(x + eps) - func(x))
#     deriv_left = 1.0 / eps * (func(x) - func(x - eps))
#     deriv_average = 0.5 * (deriv_right + deriv_left)
#     res = np.where(x > 0.5*eps, deriv_right, deriv_left)
#     res = np.where(np.logical_and(x < 0.5*eps, x > -0.5*eps), deriv_average, res)
#     return res

def plot_slice_fit(model_vol_cont: Union[pd.Series, pd.DataFrame],
                   slice: ExpirySlice,
                   is_size_filter: bool = True,
                   x_rotation: int = 90,
                   title: str = None,
                   ax: plt.Subplot = None
                   ) -> pd.Series:

    atm_points = None
    slice_t = slice.get_slice()
    plot.vol_slice_fit(bid_vol=slice_t["bid_ivs"],
                       ask_vol=slice_t["ask_ivs"],
                       model_vols=model_vol_cont,
                       title=title or 'Model fit to implied vols in strike space',
                       atm_points=atm_points,
                       is_add_mids=True,
                       x_rotation=x_rotation,
                       ax=ax)
    return model_vol_cont


def plot_plot(data: Dict,
              x_grid: np.ndarray,
              is_log_strike_xaxis: bool = False, forward: float = 1.0,
              title: str = "Title here") -> plt.Figure:
    if is_log_strike_xaxis:
        strikes = x_grid
    else:
        strikes = forward*np.exp(x_grid)
    data2 = np.array(list(data.values())).T
    columns = list(data.keys())
    data = pd.DataFrame(data2, index=strikes, columns=columns)

    with sns.axes_style(get_theme()):
        fig, axs = plt.subplots(1, 1, figsize=(10, 6), tight_layout=True)
    # title = f"phi={np.real(phi):0.2f}+{np.imag(phi):0.2f}i"
    # fig.suptitle(title, color='darkblue')
    sns.lineplot(data=data, ax=axs)
    axs.set_title(title)
    # plt.show()
    return fig


def plot_subplot(data: Dict,
                 x_grid: np.ndarray,
                 is_log_strike_xaxis: bool = False, forward: float = 1.0,
                 title: str = "Title here") -> plt.Figure:
    if is_log_strike_xaxis:
        strikes = x_grid
    else:
        strikes = forward*np.exp(x_grid)
    data2 = np.array(list(data.values())).T
    columns = list(data.keys())
    pd_data = pd.DataFrame(data2, index=strikes, columns=columns)

    sz = len(data)
    fig_size = get_fig_size(sz)
    with sns.axes_style(get_theme()):
        fig, axs = plt.subplots(sz//2, 2, figsize=fig_size, tight_layout=True)
        for idx, key in enumerate(data.keys()):
            subtitle = key
            # in case if we have one row
            if sz//2 == 1:
                ax =axs[idx % 2]
            else:
                ax = axs[idx % 2][idx // 2]
            sns.lineplot(data=pd_data[key], ax=ax)
            ax.set_title(subtitle, fontsize=12, color='darkblue')
            ax.set_ylabel("")
        fig.suptitle(title)
    return fig

def lower_bound(bmarr: np.ndarray, arr: np.ndarray):
    mapping = np.zeros_like(arr)
    N = arr.size
    M = bmarr.size
    for k in reversed(range(M)):
        for n in reversed(range(N)):
            if bmarr[k] >= arr[n]:
                mapping[n] = k
    return mapping

class Discount:
    def __init__(self, today: datetime, r: float):
        self.today = today
        self.r = r

    def df(self, d):
        year_frac = to_yearfrac(self.today, d)
        disc_factor = np.exp(-self.r * year_frac)
        return disc_factor


class Forward:
    def __init__(self, today, df: Discount, s0):
        self.today = today
        self.df = df
        self.s0 = s0

    def get_forward(self, d: datetime):
        disc_factor = self.df.df(d)
        return self.s0 / disc_factor

    def get_forwards(self, dates: np.ndarray):
        forwards = np.zeros(len(dates))
        for idx, d in enumerate(dates):
            forwards[idx] = self.get_forward(d)
        return forwards