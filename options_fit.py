import matplotlib.pyplot as plt
import numpy as np
from enum import Enum

import pandas as pd
from scipy.stats import norm
import seaborn as sns
from scipy.optimize import curve_fit
from scipy.optimize import minimize
from typing import Dict, Literal
from experimental import eutils
from experimental import implied_vol

# internal
from stochvolmodels.data.test_option_chain import get_btc_test_chain_data
from stochvolmodels.data.option_chain import OptionChain
from stochvolmodels.utils import plots as plot
from prop.data.deribit.dbt_chain_fitter import Underlying, read_deribit_cvs_data
import prop.generic_vol_slices as gsv


def fit_static_data(reuse_prev_result: bool = False):
    chain_data = get_btc_test_chain_data()
    fit_chain_data(chain_data=chain_data, reuse_prev_result=reuse_prev_result)


def fit_at_date(ticker, fetch_time, reuse_prev_result):
    selected_slices = None
    slices = read_deribit_cvs_data(ticker=ticker, fetch_time=fetch_time)
    chain = gsv.SlicesChain(slices=slices)
    bid_ivs, ask_ivs, strikes_ttms, optiontypes_ttms, ttms, forwards, discfactors = chain.select_slice_data(
        selected_slices=selected_slices)
    chain.print_slices_id()
    ids = np.array([f"{np.floor(ttm * 360):.0f}d" for ttm in ttms])
    chain_data = OptionChain(ids=ids,
                             ttms=ttms,
                             ticker=ticker,
                             forwards=forwards,
                             strikes_ttms=strikes_ttms,
                             optiontypes_ttms=optiontypes_ttms,
                             discfactors=discfactors,
                             bid_ivs=bid_ivs,
                             ask_ivs=ask_ivs)
    fit_chain_data(chain_data=chain_data, fetch_time=fetch_time, reuse_prev_result=reuse_prev_result)


def fit_chain_data(chain_data: OptionChain,
                   reuse_prev_result: bool,
                   fetch_time: str = '16_Sep_2021_20_34_25',
                   nb_pts: int = 2500):
    atm_vol = chain_data.get_chain_atm_vols()
    params = {}
    # Count number of ttms with at least 7 quoted strikes
    liq_ttms = [idx for idx, strikes in enumerate(chain_data.strikes_ttms) if strikes.size >= 7]
    nb_subplots = len(liq_ttms)
    fig_size = eutils.get_fig_size(nb_subplots)
    nb_rows = int(np.ceil(nb_subplots/2))
    with sns.axes_style(eutils.get_theme()):
        fig, axs = plt.subplots(nb_rows, 2, figsize=fig_size, tight_layout=True)

    for idx, ttm in enumerate(chain_data.ttms):
        if chain_data.strikes_ttms[idx].size < 7:
            continue
        mid_vol = 0.5 * (chain_data.ask_ivs[idx] + chain_data.bid_ivs[idx])
        term_vol = mid_vol * np.sqrt(ttm)
        forward = chain_data.forwards[idx]
        log_strikes = np.log(chain_data.strikes_ttms[idx] / forward)
        d1 = -log_strikes / term_vol + 0.5 * term_vol
        vega = np.sqrt(ttm) * np.exp(-0.5 * d1 * d1)
        sigma = np.reciprocal(vega)
        norm_strikes = log_strikes / atm_vol[-1] / np.sqrt(ttm)

        obj_func = lambda norm_strikes, c, w_c, s_c, alpha, beta, s, k: \
            implied_vol.calc_implied_vol_sigmoid(norm_strikes, c, w_c, s_c, alpha, beta, s, k, ttm, p=100)
        # initial guess
        # total_var = atm_vol[idx]
        p0 = np.array([0.0, atm_vol[idx], 0.0, 0.0, 0.0, 0.0, 0.0])
        # # test
        # obj_func(norm_strikes, *p0)
        # bounds
        bounds = ((-3, 0.0, 0.0, -10, -10, -3, -3), (3, np.inf, np.inf, 10, 10, 3, 3))
        use_lm = True
        popt, pcov = curve_fit(f=obj_func, xdata=norm_strikes, ydata=mid_vol, p0=p0, sigma=sigma, bounds=bounds, method='trf')
        if reuse_prev_result:
            if idx > 0:
                p0 = popt

        fit_params = {'c': popt[0], 'w_c': popt[1], 's_c': popt[2], 'alpha': popt[3], 'beta': popt[4],
                      's': popt[5], 'k': popt[6]}
        print(f"vol at critical point: {np.sqrt(fit_params['w_c'] / ttm):.2%}")
        # print(popt, pcov)
        cont_log_strikes = np.linspace(log_strikes[0], log_strikes[-1], nb_pts)
        cont_strikes = forward * np.exp(cont_log_strikes)
        cont_norm_strikes = cont_log_strikes / atm_vol[-1] / np.sqrt(ttm)
        model_vols_cont = pd.Series(obj_func(cont_norm_strikes,
                                             fit_params['c'],
                                             fit_params['w_c'],
                                             fit_params['s_c'],
                                             fit_params['alpha'],
                                             fit_params['beta'],
                                             fit_params['s'],
                                             fit_params['k']),
                                    index=cont_strikes, name='Model IV')

        idx2 = liq_ttms.index(idx)
        col = idx2 % 2
        row = idx2 // 2
        # in case if we have one row
        if nb_rows == 1:
            ax = axs[col]
        else:
            ax = axs[row][col]

        plot.vol_slice_fit(bid_vol=pd.Series(chain_data.bid_ivs[idx], index=chain_data.strikes_ttms[idx]),
                           ask_vol=pd.Series(chain_data.ask_ivs[idx], index=chain_data.strikes_ttms[idx]),
                           model_vols=model_vols_cont,
                           title=f"fit for ttm={chain_data.ids[idx]}",
                           atm_points=None,
                           is_add_mids=True,
                           x_rotation=0,
                           ax=ax)
        params[chain_data.ids[idx]] = fit_params

    if nb_subplots % 2 == 1:
        fig.delaxes(axs[nb_subplots // 2, nb_subplots % 2])
    print(params)

    df = pd.DataFrame(params)
    with open('mytable.tex', 'w') as tf:
        tf.write(df.to_latex())

    pd_data = pd.DataFrame(params)
    for clm in pd_data.columns:
        pd_data[clm]=pd_data[clm].map('{0:.4f}'.format)
    with open('figures//res.tex', 'w') as tf:
        tf.write(pd_data.to_latex())
    filename = chain_data.ticker + "_" + fetch_time
    path = plot.save_fig(fig=fig, local_path='figures//', file_name=filename, dpi=150)
    print(f"Succesfully saved to {path}")

    # put all slices in one plot
    data = {}
    # with sns.axes_style(eutils.get_theme()):
    #     fig, axs = plt.subplots(1, 1, figsize=fig_size, tight_layout=True)
    k_grid = np.linspace(-1.0, 1.0, nb_pts)
    for idx, ttm in enumerate(params):
        if chain_data.ttms[liq_ttms[idx]] < 0.2:
            continue
        func = sigma_func(params[ttm], scale_wc=False, sigma_atm=atm_vol[-1], ttm=chain_data.ttms[liq_ttms[idx]])
        func = func(k_grid)
        data[chain_data.ids[liq_ttms[idx]]] = func
    forward = 1.0
    fig2 = eutils.plot_plot(data=data, x_grid=k_grid, is_log_strike_xaxis=True,
                     forward=forward)
    # # plot with term structure of fitted parameters
    # with sns.axes_style('darkgrid'):
    #     fig, axs = plt.subplots(7, 1, figsize=(10, 7), tight_layout=True)
    # # make it first
    # figs.insert(0, fig)
    #
    # params = pd.DataFrame.from_dict(params, orient='index')
    # for idx, param in enumerate(params.columns.to_list()):
    #     plot.model_param_ts(param_ts=params[param], yvar_format='{:.0%}', ax=axs[idx])
    #
    # filename = chain_data.ticker + "_" + fetch_time
    # plot.fig_list_to_pdf(figs=figs, file_name=filename, local_path="figures//",
    #                      is_add_current_date=False)
    # plt.show()
    filename = chain_data.ticker + "_" + fetch_time + "_all"
    path = plot.save_fig(fig=fig2, local_path='figures//', file_name=filename, dpi=150)
    print(f"Succesfully saved to {path}")


def total_var_func(params: Dict,
                   scale_wc: bool,
                   sigma_atm: float,
                   ttm: float):
    c = params['c']
    w_c = params['w_c']
    s_c = params['s_c']
    alpha = params['alpha']
    beta = params['beta']
    s = params['s']
    kappa = params['k']
    if 'p' in params:
        p = params['p']
    else:
        p = 1000.0
    if scale_wc:
        w_c_total = w_c * ttm
    else:
        w_c_total = w_c
    obj_func_ns = lambda norm_strikes, c, w_c_total, s_c, alpha, beta, s, kappa: \
        implied_vol.calc_total_var_sigmoid(norm_strikes, c, w_c_total, s_c, alpha, beta, s, kappa, ttm, p)
    total_var = lambda k: obj_func_ns(k / sigma_atm / np.sqrt(ttm), c, w_c_total, s_c, alpha, beta, s, kappa)
    return total_var


def sigma_func(params: Dict,
               scale_wc: bool,
               sigma_atm: float,
               ttm: float):
    c = params['c']
    w_c = params['w_c']
    s_c = params['s_c']
    alpha = params['alpha']
    beta = params['beta']
    s = params['s']
    kappa = params['k']
    if 'p' in params:
        p = params['p']
    else:
        p = 1000.0
    if scale_wc:
        w_c_total = w_c * ttm
    else:
        w_c_total = w_c
    obj_func_ns = lambda norm_strikes, c, w_c_total, s_c, alpha, beta, s, kappa: \
        implied_vol.calc_implied_vol_sigmoid(norm_strikes, c, w_c_total, s_c, alpha, beta, s, kappa, ttm, p)
    sigma = lambda k: obj_func_ns(k / sigma_atm / np.sqrt(ttm), c, w_c_total, s_c, alpha, beta, s, kappa)
    return sigma


def plot_ivols(params: Dict,
               ttm: float,
               k: np.ndarray,
               scale_wc: bool = True,
               sigma_atm: float = 1.0,
               is_log_strike_xaxis: bool = False,
               forward: float = 1.0) -> plt.Figure:
    sigma = sigma_func(params=params, scale_wc=scale_wc, sigma_atm=sigma_atm, ttm=ttm)
    eps = 1e-2
    diff = lambda func, k: 0.5 * (func(k + eps) - func(k - eps)) / eps
    diff2 = lambda func, k: (func(k + eps) - 2.0 * func(k) + func(k - eps)) / eps / eps
    k_grid = k
    der1_sigma = diff(sigma, k)
    der2_sigma = diff2(sigma, k)
    data = {'sigma': sigma(k_grid), 'der1_sigma': der1_sigma, 'der2_sigma': der2_sigma}
    return eutils.plot_plot(data=data, x_grid=k_grid, is_log_strike_xaxis=is_log_strike_xaxis,
                            forward=forward)

def dens_or_L(sigma,
              k: np.ndarray,
              ttm: float,
              return_L: bool = False,
              eps: float = 1e-2):
    d1 = lambda k: -k / sigma(k) / np.sqrt(ttm) + 0.5 * sigma(k) * np.sqrt(ttm)
    d2 = lambda k: -k / sigma(k) / np.sqrt(ttm) - 0.5 * sigma(k) * np.sqrt(ttm)
    diff = lambda func, k: 0.5 * (func(k + eps) - func(k - eps)) / eps
    diff2 = lambda func, k: (func(k + eps) - 2.0 * func(k) + func(k - eps)) / eps / eps
    if return_L:
        L = lambda k: diff(d2, k) * diff(d1, k) + diff2(sigma, k) / sigma(k)
        return L
    else:
        dens = lambda k: norm.pdf(-d1(k)) * (sigma(k) * diff(d2, k) * diff(d1, k) + diff2(sigma, k))
        return dens


def plot_non_arb_region(params: Dict,
                        ttm: float,
                        k: np.ndarray,
                        scale_wc: bool = True,
                        sigma_atm: float = 1.0,
                        plot_dens: bool = False,
                        is_log_strike_xaxis: bool = False,
                        forward: float = 1.0) -> plt.Figure:
    sigma = sigma_func(params=params, scale_wc=scale_wc, sigma_atm=sigma_atm, ttm=ttm)
    L = dens_or_L(sigma=sigma, k=k, ttm=ttm, return_L=True)
    data = {'L': L(k)}
    if plot_dens:
        density = dens_or_L(sigma=sigma, k=k, ttm=ttm, return_L=False)
        data['density'] = density(k)

    return eutils.plot_subplot(data=data, x_grid=k, is_log_strike_xaxis=is_log_strike_xaxis,
                               forward=forward)


def plot_kink(params: Dict,
              ttm: float,
              k: np.ndarray,
              scale_wc: bool = True,
              sigma_atm: float = 1.0,
              is_log_strike_xaxis: bool = False,
              forward: float = 1.0,
              title="Title here") -> plt.Figure:
    data = {}
    for key in params.keys():
        param = params[key]
        eps = 1e-3
        # plot variance
        func = total_var_func(params=param, scale_wc=scale_wc, sigma_atm=sigma_atm, ttm=ttm)
        diff2 = lambda func, k: (func(k + eps) - 2.0 * func(k) + func(k - eps)) / eps / eps
        data[f"{key}: total variance"] = diff2(func, k)
        # plot implied vol
        func = sigma_func(params=param, scale_wc=scale_wc, sigma_atm=sigma_atm, ttm=ttm)
        diff2 = lambda func, k: (func(k + eps) - 2.0 * func(k) + func(k - eps)) / eps / eps
        data[f"{key}: implied volatility"] = diff2(func, k)
    return eutils.plot_subplot(data=data, x_grid=k, is_log_strike_xaxis=is_log_strike_xaxis,
                               forward=forward, title=title)

class UnitTests(Enum):
    OPTION_FIT_STATIC = 1
    OPTION_FIT_AT_DATE = 2
    PLOT_IVOLS = 3
    BUMP_ANALYSIS = 4
    ASYMP_LIMIT = 5
    SMOOTHNESS = 6
    NON_ARB_REGION = 7


def run_unit_test(unit_test: UnitTests):
    reuse_prev_result = True
    # smile with call/put wings
    baseparams = { 1.0: {'w_c': 0.3219, 's_c': 0.01, 's': 0.0013, 'k': 0.0523, 'alpha': -0.7852, 'beta': -1.6348, 'c': 0.3368},
                   # 0.125: {'w_c': 1.32, 's_c': 0.01, 's': 0.1829, 'k': 0.1398, 'alpha': -1.7123, 'beta': -1.2041, 'c': -0.3518}
                   0.125: {'w_c': 1.32, 's_c': 0.01, 's': 0.1829, 'k': 0.1398, 'alpha': -1.7123, 'beta': -1.2041, 'c': -0.3518}
                   }
    # # downward sloping smile
    # baseparams = {'c': 0.06496, 'w_c': 0.3219, 's_c': 0.0, 'alpha': 0.0, 'beta': -5.5147,
    #               's': 0.0040, 'k': 0.0014}
    sigma_atm = 1.0
    ttm = 0.125

    if unit_test == UnitTests.OPTION_FIT_STATIC:
        reuse_prev_result = True
        fit_static_data(reuse_prev_result=reuse_prev_result)
    elif unit_test == UnitTests.OPTION_FIT_AT_DATE:
        underlying = Underlying.BTC

        if underlying == Underlying.BTC:
            # for BTC
            fetch_time = '12_Aug_2022_01_13_00'
            fetch_time = '12_Aug_2022_22_01_59'
            fetch_time = '23_Aug_2022_21_40_22'
            fetch_time = '27_Aug_2022_19_51_36'
            fetch_time = '16_Sep_2021_20_34_25'
            fetch_time = '13_May_2022_20_32_35'
            # fetch_time = '29_Sep_2021_23_21_25'
            fetch_time = '08_Sep_2021_08_15_41'
            # fetch_time = '28_Aug_2022_14_29_33'

        elif underlying == Underlying.ETH:
            # for ETH
            fetch_time = '12_Aug_2022_01_13_08'
            fetch_time = '12_Aug_2022_22_02_08'
            fetch_time = '23_Aug_2022_21_40_37'
            fetch_time = '27_Aug_2022_19_52_04'
            # fetch_time = '16_Sep_2021_20_34_58'
            # fetch_time = '29_Sep_2021_23_21_09'

        fit_at_date(ticker=underlying.value,
                    fetch_time=fetch_time,
                    reuse_prev_result=reuse_prev_result)
    elif unit_test == UnitTests.PLOT_IVOLS:
        # SABR vol slice
        beta = 1
        vartheta = 2
        sigma0 = 0.8
        J = lambda y: np.sqrt(1 + vartheta ** 2 * y ** 2 - 2.0 * beta * y)
        x = lambda y: 1.0 / vartheta * np.log((J(y) * vartheta + y * vartheta ** 2 - beta) / (vartheta - beta))
        y = lambda k: -k / sigma0
        sigma = lambda k: sigma0 * y(k) / x(y(k))
        # sigmoid vol
        # set in fig 1
        # T = 8.0 / 365
        # c = 0.0
        # s_c = -0.04302
        # w_c = 0.1652
        # alpha = -0.42623
        # beta = 0.60308
        # s = -0.2
        # kappa = 1.035

        # # set in fig 2
        # T = 43.0/365
        # c = 0.5769
        # s_c = 0.0
        # w_c = 0.16775
        # alpha = -0.0004
        # beta = 2.7457
        # s = -0.003
        # kappa = 0.11

        # # set in fig 3
        # ttm = 8.0 / 365
        # c = 0.0032
        # s_c = -0.038
        # w_c = 0.0703
        # alpha = 0.5741
        # beta = -0.80175
        # s = -0.2
        # kappa = 0.99
        # sigma_atm = 0.3
        #
        # w_c_total = w_c * ttm
        # params = {'c': c, 'w_c': w_c, 's_c': s_c, 'alpha': alpha, 'beta': beta, 's': s, 'k': kappa}
        params = baseparams.copy()

        plot_ivols(params=params, ttm=ttm, scale_wc=True, sigma_atm=sigma_atm,
                   is_log_strike_xaxis=True, plot_L=True, plot_slice=False,
                   k=np.linspace(-15, 15, 2500))
        plt.show()

    elif unit_test == UnitTests.SMOOTHNESS:
        # params_kink    = {'w_c': 0.3219, 's_c': 0.01, 's': 0.0138, 'k': 0.0523, 'alpha': -0.7852, 'beta': -1.6348, 'c': 0.3368, 'p': 1000.0}
        # params_smooth2 = {'w_c': 0.3219, 's_c': 0.01, 's': 0.0138, 'k': 0.0523, 'alpha': -0.7852, 'beta': -1.6348, 'c': 0.3368, 'p': 3.0}
        # params_smooth1 = {'w_c': 0.3219, 's_c': 0.01, 's': 0.0138, 'k': 0.0523, 'alpha': -0.7852, 'beta': -1.6348, 'c': 0.3368, 'p': 1.0}
        # params_smooth0 = {'w_c': 0.3219, 's_c': 0.01, 's': 0.0, 'k': 0.0, 'alpha': -0.7852, 'beta': -1.6348, 'c': 0.3368}
        params_kink = baseparams[ttm].copy()
        params_smooth = baseparams[ttm].copy()
        params_smooth['p'] = 1.0
        params = {'(A)': params_kink,
                  '(B)': params_smooth
                 }
        k_range = np.linspace(-2, 2, 5000)
        fig = plot_kink(params=params, ttm=ttm, scale_wc=True, sigma_atm=sigma_atm,
                        is_log_strike_xaxis=True, k=k_range, title="Second derivatives $w''$ and $\sigma''$ ")
        file_name = f"der2_{np.ceil(ttm * 365):.0f}d"
        plot.save_fig(fig=fig, local_path='figures//', file_name=file_name, dpi=150)
        plt.show()

    elif unit_test == UnitTests.NON_ARB_REGION:
        params = baseparams[ttm].copy()
        params['p'] = 1
        k_range = np.linspace(-1, 1, 2500)
        fig = plot_non_arb_region(params=params, ttm=ttm, scale_wc=True, sigma_atm=sigma_atm,
                                  is_log_strike_xaxis=True, plot_dens=True,
                                  k=k_range)
        file_name = f"durrleman_region_{np.ceil(ttm * 365):.0f}d"
        plot.save_fig(fig=fig, local_path='figures//', file_name=file_name, dpi=150)
        plt.show()

    elif unit_test == UnitTests.BUMP_ANALYSIS:
        bumps = {'c': [-0.3, 0.3],
                 'w_c': [-0.02, 0.02],
                 's_c': [-0.03, 0.03],
                 'alpha': [-0.2, 0.2],
                 'beta': [-0.2, 0.2],
                 's': [-0.03, 0.03],
                 'k': [-0.03, 0.03]}
        # params_bump['w_c'] = params_bump['w_c'] + 0.01
        # params_bump['s_c'] = params_bump['s_c'] + 0.03
        # params_bump['alpha'] = params_bump['alpha'] - 0.2 # TODO: also + 0.2
        # params_bump['beta'] = params_bump['beta'] - 0.2  # TODO: also + 0.2
        # params_bump['s'] = params_bump['s'] - 0.03
        # params_bump['k'] = params_bump['k'] + 0.03

        k_grid = np.linspace(-2.0, 2.0, 500)
        # plot in strike, not moneyness axis
        forward = 1.0
        strikes = forward * np.exp(k_grid)

        is_density = False
        return_L = False
        params = baseparams[ttm]
        subplot_idx = ['A', 'B', 'C', 'D', 'E', 'F', 'G']

        sigma_base = sigma_func(params=params, scale_wc=True, sigma_atm=sigma_atm, ttm=ttm)
        with sns.axes_style(eutils.get_theme()):
            fig, axs = plt.subplots(4, 2, figsize=(12, 16), tight_layout=True)
        for idx, param in enumerate(bumps.keys()):
            if is_density:
                file_name = f"impact_of_parameters_dens"
                dens = dens_or_L(sigma=sigma_base, k=k_grid, ttm=ttm, return_L=return_L)
                data = {'base': dens(k_grid)}
            else:
                file_name = f"impact_of_parameters"
                data = {'base': sigma_base(k_grid)}
            for bump in bumps[param]:
                params_bump_single = params.copy()
                params_bump_single[param] = params_bump_single[param] + bump
                sigma_bump = sigma_func(params=params_bump_single, scale_wc=True, sigma_atm=sigma_atm, ttm=ttm)
                if is_density:
                    dens = dens_or_L(sigma=sigma_bump, k=k_grid, ttm=ttm, return_L=return_L)
                    data[f"bump: {bump: .0%}"] = dens(k_grid)
                else:
                    data[f"bump: {bump: .0%}"] = sigma_bump(k_grid)
            # transform to dataframe
            data2 = np.array(list(data.values())).T
            columns = list(data.keys())
            pd_data = pd.DataFrame(data2, index=k_grid, columns=columns)
            col = idx % 2
            row = idx // 2
            ax = axs[row][col]
            sns.lineplot(data=pd_data, ax=ax)
            ax.set_title(f"({subplot_idx[idx]}) Impact of {param.upper()}")
        nb_max = len(bumps)
        # if we have odd number of subplots, delete the last one
        if nb_max % 2 == 1:
            fig.delaxes(axs[nb_max // 2, nb_max % 2])
        file_name = file_name + f"_{np.ceil(ttm*365):.0f}d"
        plot.save_fig(fig=fig, local_path='figures//', file_name=file_name, dpi=150)

        plt.show()

    elif unit_test == UnitTests.ASYMP_LIMIT:
        params = baseparams[ttm].copy()
        # params = {'c': 0.3368, 'w_c': 0.1652, 's_c': -0.04302, 'alpha': -0.4262, 'beta': 0.603, 's': -0.2, 'k': 1.035}
        # ttm = 8.0 / 365
        sigma_base = sigma_func(params=params, scale_wc=True, sigma_atm=sigma_atm, ttm=ttm)
        k_grid = np.linspace(-5.0, 5.0, 150)
        forward = 1.0
        is_log_strike_xaxis = False
        c = params['c']
        w_c = params['w_c']
        s_c = params['s_c']
        alpha = params['alpha']
        beta = params['beta']
        s = params['s']
        kappa = params['k']
        absalpha = np.fabs(alpha)
        absbeta = np.fabs(beta)
        wplus = lambda y: w_c * ttm + np.sqrt(ttm) * (kappa - s * absbeta) / (beta ** 2) * y
        wminus = lambda y: w_c * ttm - np.sqrt(ttm) * (kappa + s * absalpha) / (absalpha ** 2) * y
        z_grid = k_grid / sigma_atm / np.sqrt(ttm)
        y_grid = z_grid - c
        wplus_vals = wplus(y_grid)
        wminus_vals = wminus(y_grid)
        sigma_plus = np.sqrt(wplus_vals/ttm)
        sigma_minus = np.sqrt(wminus_vals / ttm)
        sigma = sigma_base(k_grid)
        sigma_plus = np.where(k_grid <= 0.0, np.nan, sigma_plus)
        sigma_minus = np.where(k_grid >= 0.0, np.nan, sigma_minus)
        data = {'sigma': sigma , 'sigma_plus': sigma_plus, 'sigma_minus': sigma_minus}
        fig = eutils.plot_plot(data=data, x_grid=k_grid, is_log_strike_xaxis=True,
                               forward=forward, title='Asymptotic behaviour of $\sigma(k)$, $k\\to\pm\infty$ ')
        plot.save_fig(fig=fig, local_path='figures//', file_name="asymp_behaviour", dpi=150)
        plt.show()


if __name__ == '__main__':
    unit_test = UnitTests.OPTION_FIT_AT_DATE
    run_unit_test(unit_test=unit_test)
