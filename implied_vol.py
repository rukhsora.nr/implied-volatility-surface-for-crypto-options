from experimental import eutils
import numpy as np
import scipy.special as sci_spec
import datetime

from abc import ABC, abstractmethod


class ImpliedVol(ABC):
    def __init__(self):
        super(ImpliedVol, self).__init__()

    @abstractmethod
    def _get_implied_vol_at_slice(self, k: np.ndarray,
                                  slice_date: datetime.date, **kwargs) -> np.ndarray:
        raise NotImplementedError(f"must be implemented in parent class")

    @abstractmethod
    def _interpolate_in_moneyness(self, k: np.ndarray, expiry: datetime, slice1, slice2, *kwarg) -> np.ndarray:
        raise NotImplementedError(f"must be implemented in parent class")

    @abstractmethod
    def _interpolate_in_time(self, time1: float, time2: float,
                             vol1: np.ndarray, vol2: np.ndarray, time: float, *kwarg) -> np.ndarray:
        raise NotImplementedError(f"must be implemented in parent class")

    @abstractmethod
    def get_implied_vol(self, expiries, k_grid: np.ndarray, *kwarg) -> np.ndarray:
        raise NotImplementedError(f"must be implemented in parent class")


def calc_implied_vol(z: np.ndarray, beta0: float, beta1: float, beta2: float) -> np.ndarray:
    imp_vol = beta0 + beta1 * z + beta2 * np.power(z, 2.0)
    return imp_vol


def calc_total_var_sigmoid(z: np.ndarray,
                           c: float,
                           w_c: float,
                           s_c: float,
                           alpha: float,
                           beta: float,
                           s: float,
                           k: float,
                           ttm: float,
                           p: float) -> np.ndarray:
    y = z - c
    F_y = y * np.tanh(p * y)
    Y_y = np.where(y > 0, 1.0 / beta * sci_spec.erf(-beta * y) if np.fabs(beta) > 1e-6 else -2.0/np.sqrt(np.pi) * y,
                   1.0 / alpha * sci_spec.erf(-alpha * y) if np.fabs(alpha) > 1e-6 else -2.0/np.sqrt(np.pi) * y)
    Y_y_sq = np.power(Y_y, 2.0)
    total_var = w_c + s_c * y / (1.0 + np.power(y, 2.0)) + F_y * np.sqrt(ttm) * (s * Y_y + k * Y_y_sq)
    return total_var


def calc_implied_vol_sigmoid(z: np.ndarray,
                             c: float,
                             w_c: float,
                             s_c: float,
                             alpha: float,
                             beta: float,
                             s: float,
                             k: float,
                             ttm: float,
                             p: float) -> np.ndarray:
    total_var = calc_total_var_sigmoid(z=z, ttm=ttm, c=c, w_c=w_c, s_c=s_c, alpha=alpha, beta=beta, s=s, k=k, p=p)
    implied_vol = np.sqrt(total_var/ttm)
    return implied_vol


class SigmoidVol:
    def __init__(self,
                 df: eutils.Discount,
                 forward: eutils.Forward,
                 beta0: float,
                 beta1: float,
                 beta2: float,
                 slice_date: datetime.date):
        self.beta0 = beta0
        self.beta1 = beta1
        self.beta2 = beta2
        self.slice_date = slice_date
        self.forward = forward


class VLVol:
    def __init__(self,
                 df: eutils.Discount,
                 forward: eutils.Forward,
                 atmvol: float,
                 skew: float,
                 smile: float,
                 beta: float,
                 slice_dates: np.ndarray
                 ):
        self.atmvol = atmvol
        self.skew = skew
        self.smile = smile
        self.beta = beta
        self.slice_dates = slice_dates

        self.alpha = -0.6
        self.c2 = 10
        self.callwing = 0
        self.callSParam = 0
        self.wingPower = 3
        self.sParamPower = 4
        self.FLOOR = 0.0001
        # self.df = df
        self.forward = forward
        self.forwards_at_slice_dates = self.forward.get_forwards(self.slice_dates)

    def __solve_quadratic(self, a, b, c):
        d = b ** 2 - 4.0 * a * c
        root = 0.5 * (-b + np.sqrt(d)) / a
        root = np.maximum(root, self.FLOOR)
        return root

    def _get_implied_vol_at_slice(self, k: np.ndarray,
                                  slice_date: datetime.date, **kwargs) -> np.ndarray:
        T = eutils.to_yearfrac(self.forward.today, slice_date)
        # k is forward moneyness
        T = np.maximum(T, 5.0 / 252)  # time is measured in years and capped at 1 week
        kappa = np.tanh(self.beta * k) / (self.beta * np.sqrt(T))
        b = -(np.power(self.atmvol, 2) + 2 * self.atmvol * self.skew * kappa)
        c = -self.smile * np.power(kappa, 2)
        d = self.callwing
        f = self.callSParam
        kappawing = np.power(kappa ** 2, self.wingPower / 2)
        kappasparam = np.power(kappa ** 2, self.sParamPower / 2)
        d = d * (-kappawing)
        f = f * (-kappasparam)
        varGuess = self.atmvol ** 2 * np.ones_like(k)
        var = varGuess
        for i in range(len(varGuess)):
            if b[i] * b[i] - 4 * c[i] > 0:
                varGuess[i] = self.__solve_quadratic(1, b[i], c[i])
            d[i] = d[i] + f[i]
            var[i] = np.maximum(self.__solve_quadratic(1.0, b[i] + d[i] / (varGuess[i] * varGuess[i]), c[i]),
                                self.FLOOR)
        iv = np.sqrt(var)
        return iv

    def _interpolate_in_moneyness(self, k: np.ndarray, expiry: datetime, slice1, slice2, *kwarg) -> np.ndarray:
        date1 = self.slice_dates[slice1]
        date2 = self.slice_dates[slice2]
        ttexp = eutils.to_yearfrac(self.forward.today, expiry)
        ttexp1 = eutils.to_yearfrac(self.forward.today, date1)
        ttexp2 = eutils.to_yearfrac(self.df.today, date2)
        if np.fabs(ttexp - ttexp1) < 1e-6:
            vols = self._get_implied_vol_at_slice(k, expiry)
        fwd1 = self.forward.get_forward(date1)
        fwd2 = self.forward.get_forward(date2)
        fwd_exp = self.forward.get_forward(expiry)
        # Interpolate in strike dimension
        kLo = np.log(k / fwd_exp) * np.sqrt(ttexp1 / ttexp)
        kHi = np.log(k / fwd_exp) * np.sqrt(ttexp2 / ttexp)
        volsLo = self._get_implied_vol_at_slice(kLo, date1)
        volsHi = self._get_implied_vol_at_slice(kHi, date2)
        vols = np.zeros_like(k)
        for idx, strk in k:
            vols[idx] = self._interpolate_in_time(ttexp1, ttexp2, volsLo[idx], volsHi[idx], ttexp)
        return vols

    def _interpolate_in_time(self, time1: float, time2: float,
                             vol1: np.ndarray, vol2: np.ndarray, time: float, *kwarg) -> np.ndarray:
        forward_var = (vol2 ** 2 * time2 - vol1 ** 2 * time1) / (time2 - time1)
        if forward_var > 0:
            forward_vol = np.sqrt(forward_var)
        else:
            forward_vol = 1e-9
        alpha_adj = self.alpha * np.tanh(self.c2 * np.fabs(forward_vol - vol1))
        if abs(alpha_adj) < 1e-9:
            lambda_ = (time - time1) / (time2 - time1)
        else:
            lambda_ = (np.exp(-alpha_adj * (time - time1) / (time2 - time1)) - 1) / (np.exp(-alpha_adj) - 1)
        vol_interp = np.sqrt((vol1 ** 2 * time1 + (vol2 ** 2 * time2 - vol1 ** 2 * time1) * lambda_) / time)
        return vol_interp

    def get_implied_vol(self, expiries, k_grid: np.ndarray, *kwarg) -> np.ndarray:
        toSliceMap = eutils.lower_bound(self.slice_dates, expiries)
        nb_strikes = k_grid.shape[1]
        nb_times = k_grid.shape[0]
        if expiries.size != nb_times:
            raise ValueError('Number of expiries and columns in strike grid are not consistent')
        iv_grid = np.zeros((expiries.size, nb_strikes))
        for i, expiry in enumerate(expiries):
            isExpiryOnSlice = self.slice_dates[toSliceMap[i]] == expiry
            fwd_at_expiry = self.forward.get_forward(expiry)
            strikes = k_grid[i, :]
            k = np.log(strikes / fwd_at_expiry)
            if isExpiryOnSlice:
                iv = self._get_implied_vol_at_slice(k, expiry)
            else:
                slice_idx_1 = toSliceMap(i) - 1
                slice_idx_2 = toSliceMap(i)
                iv = self._interpolate_in_moneyness(k, expiry, slice_idx_1, slice_idx_2)
            iv_grid[i, :] = iv
        return iv_grid
